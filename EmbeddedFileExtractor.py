#!/usr/bin/env python
"""
  Extracts embedded files from PDFs

  Author: @XOR_Hex

  Makes use of:
      Didier Stevens - pdf-parser: https://blog.didierstevens.com/programs/pdf-tools/
      Jose Miguel Esparza - peepdf : http://eternal-todo.com/tools/peepdf-pdf-analysis-tool

  Public Domain: Use at your own risk
"""


import argparse
import sys

class OptParseEmulator(object):
    '''
        Summary: Create an object with the parameters's pdf-parser expects for
            extracting embedded documents.

        filter = [True|False]
        generate = [True|False]
        verbose = [True|False]
        extract = filename to extract malformed content to
        object = id of indirect object to select (version independent)
        dump = filename to dump stream content to
        nocanonicalizedoutput = [True|False]
        debug = [True|False]
        hash = [True|False]
        content = [True|False]
    '''

    def __init__(self, _filter, generate, verbose, extract, _object, dump,
                 nocanonicalizedoutput, debug, _hash, content):
        self.filter = _filter
        self.generate = generate
        self.verbose = verbose
        self.extract = extract
        self.object = _object
        self.dump = dump
        self.nocanonicalizedoutput = nocanonicalizedoutput
        self.debug = debug
        self.hash = _hash
        self.content = content


class AutoExtractor(object):
    '''
      Locate and extract embedded documents in a PDF

        _file: The PDF file to analyze
    '''
    def __init__(self, _file):
        self._file = _file

    def extract(self):
        '''
          Do the actual work to locate and extract
        '''
        # Confirm file type. Must be a PDF
        file_types = self._get_file_type_by_yara_rule()

        if 'pdf' in map(str, file_types):
            #Check to see if there are any embedded documents
            for embedded_id in self._get_embedded_object_ids():
                # Emulate the options passed to Didier Stevens' pdf-parser.py
                options = OptParseEmulator(True, False, True, None, str(embedded_id),
                                           'extracted_'+str(embedded_id)+'.file', False,
                                           False, False, False)
                # Extract file
                self._embedded_extraction(options)


    def _embedded_extraction(self, options):
        """
          Lifted just what was needed from pdf-parser.py to extract file
        """
        oPDFParser = cPDFParser(self._file, options.verbose, options.generate)
        #selectIndirectObject = True
        while True:
            _object = oPDFParser.GetObject()
            if _object != None:
                if _object.type == PDF_ELEMENT_INDIRECT_OBJECT:
                    if _object.id == eval(options.object):
                        PrintObject(_object, options)
            else:
                break

    def _get_embedded_object_ids(self):
        """
          Using peepdf, find all objects with embedded files
        """
        pdf_parser = PDFParser()
        ret, pdf = pdf_parser.parse(self._file, True, False, False)
        stats_dict = pdf.getStats()
        embedded_files = []
        for version in stats_dict.get('Versions'):
            embedded_files.extend(version.get('Elements').get('/EmbeddedFile'))
        return embedded_files

    def _get_file_type_by_yara_rule(self):
        """
          Making use of an embedded yara rule, check to see if file is of type: PDF
          $magic aquired from:
              https://github.com/hiddenillusion/AnalyzePDF/blob/master/pdf_rules.yara
        """
        rules = yara.compile(source='rule pdf: bar {strings: $magic = { 25 50 44 46 }'+
                             'condition: $magic}')
        matches = rules.match(self._file, timeout=60)
        return matches

if __name__ == '__main__':
    '''
      Check to make sure the necessary imports can be found
    '''
    try:
        from pdfparser import cPDFParser, PrintObject, PDF_ELEMENT_INDIRECT_OBJECT
    except Exception as ex:
        print "Didier Stevens pdf-parser.py can't found."
        print "  https://raw.githubusercontent.com/DidierStevens/DidierStevensSuite/master/pdf-parser.py"
        print "  Remember to rename pdf-parser.py to pdfparser.py so it can be imported."
        sys.exit(1)

    try:
        from peepdf.PDFCore import PDFParser
    except Exception as ex:
        print "Can't find peepdf."
        print "  pip install peepdf"
        sys.exit(1)

    try:
        import yara
    except Exception as ex:
        print "Can't find yara."
        print "  pip install yara-python"
        sys.exit(1)

    '''
      Parse the arguments.  Only expect one, the file.
    '''
    parser = argparse.ArgumentParser(usage='Usage: EmbeddedFileExtractor.py PDF_file',
                                     description='Author: XOR Hex Version: 0.1')

    parser.add_argument('file')
    try:
        args = parser.parse_args()
    except IOError, msg:
        parser.error(str(msg))

    '''
      Do the auto-extraction
    '''
    extractor = AutoExtractor(args.file)
    extractor.extract()
